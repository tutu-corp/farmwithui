# FarmWithUI
FarmWithUI is an application to calculate how much a farmer can save.

## General info
This project is based on an OpenClassroom tutorial. During my training, I have learned to create an app named JoeFarm. Created about the story of one farmer who wants to know how much money they can save. I have used this training to make an app in swiftUI.

#### This Project is created with :
* Swift 5
* IOS 16.4
* Xcode 14.0

## Screenshot
|Home|Activity View|ConfirmQuantity|MoneySheet|
|--|--|--|--|
|![Home](./captures/home.png)|![ActivityView](./captures/activityView.png)|![ConfirmQuantity](./captures/confirmQuantity.png)|![MoneySheet](./captures/moneySheet.png)|

## Credit image
* Elivelton Nogueira de Pixabay
* Alexas_Fotos de Pixabay (Wheat)
* David Mark de Pixabay (Wool) 
* Michal Jarmoluk de Pixabay (Champs)
* JackieLou DL de Pixabay 
* Mostafa Elturkey de Pixabay

## Projects statut
I edit this app when i learn something which can improve it, so it's never over 🙂
