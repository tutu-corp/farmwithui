//
//  FarmWithUIApp.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 14/04/2023.
//
//  

import SwiftUI

@main
struct FarmWithUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
