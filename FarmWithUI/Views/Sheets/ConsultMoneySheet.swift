//
//  ConsultMoneySheet.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 11/05/2023.
//
//  

import SwiftUI

struct ConsultMoneySheet: View {
    
    let image = "Harvest"
    let title = "Vos économies"
    @Binding var money: Double
    
    var body: some View {
            ZStack{
                Background(image: image, circle: false, textTitleContent: title, backgroundCardMode: .complete)
                VStack{
                    Text("Vos économies sont de \(money, specifier: "%.2f") euros !")
                }
               
        }
    }
}

struct ConsultMoneySheet_Previews: PreviewProvider {
    static var previews: some View {
        ConsultMoneySheet(money: .constant(0.0))
    }
}
