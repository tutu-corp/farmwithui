//
//  ConsultBarnSheet.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 11/05/2023.
//
//  

import SwiftUI
import AlertMessage

struct ConsultBarnSheet: View {
    
    let image = "Harvest"
    let title = "Ma grange"
    
    @Binding var barn: Barn
    @Binding var money: Double
    
    @State var confirmAlert = false
    @State var showSellConfirm = false
    
    @State var howMuchDidSell: Double = 0

    // Display Snackbar
    @EnvironmentObject var snackbarSettings: SnackbarSettings
    
    var body: some View {
        
        ZStack {
            Background(image: image, circle: false, textTitleContent: title, backgroundCardMode: .complete)
            //            VStack {
            contentBarn
            //            }
                .padding()
                .padding(.horizontal)
            Spacer()
                .alert("Voulez vous vendre vos produits ?", isPresented: $confirmAlert) {
                    Button("Confirmer") {
                        sellAndReset(sellMilk: calculateMilk(), sellWheat: calculateWheat(), sellWool: calculateWool())
//                        showSellConfirm = true
                        snackbarSettings.isActive = true
                        snackbarSettings.content = "Vos marchandises ont été vendues et \(howMuchDidSell) euros ont été ajoutés à vos économies"
                        
                    }
                    Button("Annuler", role: .cancel) {
                        
                    }
                } message: {
                    Text("Votre grange sera remise à zéro et le montant de la vente sera ajouté à vos économies")
                }
                .confirmationDialog("add product: Wool", isPresented: $showSellConfirm){
                    
                } message: {
                    Text ("Vos marchandises ont été vendues et \(howMuchDidSell, specifier: "%.2f") euros ont été ajoutés à vos économies")
                }
            
        }
        .alertMessage(isPresented: $snackbarSettings.isActive, type: .snackbar) {
            Snackbar(content: snackbarSettings.content)
        }
        .environmentObject(snackbarSettings)
    }
}


extension ConsultBarnSheet {
    private var contentBarn: some View {
        Group{
            if barn.milk.quantity == 0 && barn.wheat.quantity == 0 && barn.wool.quantity == 0 {
                Text("Votre grange est vide")
            } else {
                VStack{
                    Spacer()
                    Text("Votre grange contient :")
                    if barn.milk.quantity > 0 {
                        Text("\(barn.milk.quantity, specifier: "%.2f") litres de lait")
                    }
                    if barn.wheat.quantity > 0 {
                        Text("\(barn.wheat.quantity, specifier: "%.2f") tonnes de blé")
                    }
                    if barn.wool.quantity > 0 {
                        Text("\(barn.wool.quantity, specifier: "%.2f") kilos de laine")
                    }
                    Spacer()
                    Button {
                        confirmAlert.toggle()
                    } label: {
                        Card(text: "Vendre mes produits")
                            .foregroundColor(.primary)
                            .frame(maxHeight: 70)
                    }
                    Spacer()
                        .frame(height: 20)
                }
            }
        }
    }
}

extension ConsultBarnSheet {
    
    /// Calculate value of product in barn
    /// - Returns: Double
    
    func calculateMilk() -> Double {
        let sellMilk = barn.milk.quantity * barn.milk.price
        return sellMilk
    }
    
    func calculateWheat() -> Double {
        let sellWheat = barn.wheat.quantity * barn.wheat.price
        return sellWheat
    }
    
    func calculateWool() -> Double {
        let sellWool = barn.wool.quantity * barn.wool.price
        return sellWool
    }
    
    /// Sell and reset barn
    ///
    /// - Parameters:
    ///  - sellMilk : Double,
    ///  - sellWheat : Double,
    ///  - sellWool : Double
    
    func sellAndReset(sellMilk: Double, sellWheat: Double, sellWool: Double){
        let totalSellProduct = sellMilk + sellWheat + sellWool
        money += totalSellProduct
        barn.milk.quantity = 0
        barn.wheat.quantity = 0
        barn.wool.quantity = 0
        
        howMuchDidSell = totalSellProduct
        
    }
}

struct ConsultBarnSheet_Previews: PreviewProvider {
    static var previews: some View {
        ConsultBarnSheet(barn: .constant(Barn()), money: .constant(0.0))
            .environmentObject(SnackbarSettings())
    }
}
