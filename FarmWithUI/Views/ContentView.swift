//
//  ContentView.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 14/04/2023.
//
//  

import SwiftUI

struct ContentView: View {
    
    @StateObject var contentViewModel = ContentViewModel()
    @StateObject var snackbarSettings = SnackbarSettings()
    @State var farm = Farm()
    
    @State private var showMoneySheet = false
    @State private var showBarnSheet = false
    
    var body: some View {
        NavigationView {
            ZStack {
                Background(image: "Barn", circle: true, textTitleContent: "Ma ferme", backgroundCardMode: .short,spacerTop: true)
                VStack{
                    Spacer().frame(height: 200)
                    
                    ActivityButtonCard
                    
                    BarnButtonCard
                    
                    MoneyButtonCard
                }
                .padding(.horizontal)
            }
        }
        .environmentObject(contentViewModel)
        .environmentObject(snackbarSettings)
    }
}

extension ContentView {
    
    private var ActivityButtonCard: some View {
        NavigationLink {
            ActivityView(farm: $farm)
        } label: {
            Card(text: "Enregistrer une activité")
                .foregroundColor(.primary)
        }
    }
    
    private var BarnButtonCard: some View {
        Group {
            Button{
                showBarnSheet.toggle()
            } label: {
                Card(text: "Consulter ma grange")
                    .foregroundColor(.primary)
            }
            
            .sheet(isPresented: $showBarnSheet) {
                ConsultBarnSheet(barn: $contentViewModel.barn, money: $farm.money)
            }
        }
    }
    
    private var MoneyButtonCard: some View {
        Group {
            Button {
                showMoneySheet.toggle()
            } label: {
                Card(text: "Consulter mes économies")
                    .foregroundColor(.primary)
                    .frame(maxHeight: 70)
            }
            .sheet(isPresented: $showMoneySheet) {
                ConsultMoneySheet(money: $farm.money)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
