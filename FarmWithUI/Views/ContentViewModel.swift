//
//  ContentViewModel.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 26/05/2023.
//
//  

import Foundation

class ContentViewModel: ObservableObject {
    @Published var barn: Barn = Barn()
}
