//
//  Snackbar.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 26/05/2023.
//
//  

import SwiftUI

struct Snackbar: View {
    
    var content : String
    
    var body: some View {
        HStack {
            Spacer()
            Image(systemName: "checkmark.seal")
                .resizable()
                .frame(width: 35, height: 35)
                .foregroundColor(.white)
                .padding()
            
            Text(content)
                .foregroundColor(.white)
            
            Spacer()
        }
        .background(Color.green)
    }
}

struct Snackbar_Previews: PreviewProvider {
    static var previews: some View {
        Snackbar(content: "Snackbar")
    }
}
