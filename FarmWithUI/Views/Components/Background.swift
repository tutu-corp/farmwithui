//
//  Background.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 25/04/2023.
//
//  

import SwiftUI

struct Background: View {

    enum cardMode {
        case complete, short
    }
    
    var image: String
    var circle: Bool
    var textTitleContent: String
    var backgroundCardMode: cardMode?
    var subtitle: String?
    var spacerTop: Bool = false
    
    // Subtitle storage
    var storage: Bool = false
    var storageMoney : Double = 0
    var storageMilk : Double = 0
    var storageWool : Double = 0
    var storageWheat : Double = 0
    
    
    var body: some View {
        VStack {
            
            SpacerIfBackIsHidden
            ZStack {
                
                ChoiceBackgroundCardMode
                VStack{
                    
                    ChoiceCircle
                    MainTitle
                    SubtitleStorageContent
                    Spacer()
                }
                .offset(y: -30)
            }
            .padding(.top, 30.0)
        }
        .background(Image(image)
            .resizable()
            .scaledToFill()
            .ignoresSafeArea())
    }
}

extension Background {
    
    private var SpacerIfBackIsHidden : some View {
        Group{
            if spacerTop == true {
                Spacer()
                    .frame(height: 40)
            }
        }
    }
    
    private var ChoiceBackgroundCardMode : some View {
        Group {
            // Type of card background
            if backgroundCardMode == .complete {
                RoundedRectangle(
                    cornerSize: CGSize(width: 15, height: 15)
                )
                .shadow(radius: 1)
                .foregroundColor(.white)
                .padding()
            } else if backgroundCardMode == .short {
                VStack{
                    RoundedRectangle(
                        cornerSize: CGSize(width: 15, height: 15)
                    )
                    .frame(height: 160)
                    .shadow(radius: 1)
                    .foregroundColor(.white)
                    .padding()
                    Spacer()
                }
            }
        }
    }
    
    private var ChoiceCircle : some View {
        Group{
            if circle == true {
                CircleImage(image: image)
                    .frame(width: 100, height: 100)
            } else {
                Spacer()
                    .frame(height: 100)
            }
        }
    }
    
    private var MainTitle : some View {
        Text(textTitleContent)
            .font(.title)
    }
    
    private var SubtitleStorageContent : some View {
        Group {
            if storage == true {
                Divider()
                    .padding(.horizontal)
                HStack{
                        Image(systemName: "eurosign.circle")
                        Text("\(storageMoney, specifier: "%.2f")")
                    Group{
                        Image(systemName: "takeoutbag.and.cup.and.straw")
                        Text("\(storageMilk, specifier: "%.2f")")
                    }
                    Group{
                        Image(systemName: "cloud")
                        Text("\(storageWool, specifier: "%.2f")")
                    }
                    Group{
                        Image(systemName: "laurel.trailing")
                        Text("\(storageWheat, specifier: "%.2f")")
                    }
                }
                .foregroundColor(/*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                
            }
        }
    }
}

struct Background_Previews: PreviewProvider {
    static var previews: some View {
        Background(image: "Harvest", circle: true, textTitleContent: "Preview", backgroundCardMode: .complete, spacerTop: true)
        
        Background(image: "Harvest", circle: true, textTitleContent: "Preview", backgroundCardMode: .short, subtitle: "Preview Subtitle",storage: true)
        
        Background(image: "Harvest", circle: true, textTitleContent: "Preview")
        
        Background(image: "Harvest", circle: true, textTitleContent: "Preview", subtitle: "Preview Subtitle", storage: true)
    }
    
}
