//
//  CircleImage.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 14/04/2023.
//
//  

import SwiftUI

struct CircleImage: View {
    
    var image : String
    
    var body: some View {
        Image(image)
            .resizable()
            .aspectRatio(1, contentMode: .fit)
            .clipShape(Circle())
            .overlay{
                Circle().stroke(.white, lineWidth: 2)
            }
            .shadow(radius: 4)
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage(image: "Champs")
        
        CircleImage(image: "scrump")
    }
}
