//
//  Card.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 25/04/2023.
//
//  

import SwiftUI

struct Card: View {
    
//    var horizontalAlign: Bool
    var image: String?
    var text: String
    var active: Bool = true

    
    var body: some View {
        ZStack{
            if active == true {
                RoundedRectangle(cornerSize: CGSize(width: 15, height: 15))
                    .shadow(radius: 3)
                .foregroundColor(.white)
            } else {
                RoundedRectangle(cornerSize: CGSize(width: 15, height: 15))
                    .shadow(radius: 3)
                    .foregroundColor(.gray)
            }
                HStack{
                    if image != nil {
                        CircleImage(image: image!)
                            .padding(5)
                            .frame(maxHeight: 100)
                    } else {
                        Spacer()
                    }
                    Text(text)
                        .multilineTextAlignment(.center)
                    Spacer()
                }
        }
        .frame(maxHeight: 70)
    }
}

struct Card_Previews: PreviewProvider {
    static var previews: some View {
        Card(image: "Champs", text: "Preview", active: false)
        Card(image: "Champs", text: "Preview")
    }
}
