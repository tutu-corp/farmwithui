//
//  ProductButtonCard.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 26/05/2023.
//
//  

import SwiftUI

struct ProductButtonCard<T: Product>: View {
    
    @Binding var showPicker: Bool
    @Binding var product: T
    @EnvironmentObject var contentViewModel: ContentViewModel
    
    
    var body: some View {
            Button {
                withAnimation{
                    showPicker.toggle()
                }
            } label: {
                Card(image: product.image, text: product.action)
                    .foregroundColor(.primary)
            }
            
            if showPicker == true {
                    PickerProduct(barn: $contentViewModel.barn, product: $product) {
                        showPicker.toggle()
                    }
                    .transition(.scale)
            }
    }
}

struct ProductButtonCard_Previews: PreviewProvider {
    static var previews: some View {
        ProductButtonCard(showPicker: .constant(true), product: .constant(Milk(price: 0.5)))
            .environmentObject(ContentViewModel())
    }
}
