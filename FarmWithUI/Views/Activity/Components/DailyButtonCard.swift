//
//  DailyButtonCard.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 26/05/2023.
//
//  

import SwiftUI

struct DailyButtonCard: View {
    
    
    @Binding var money: Double
    @Binding var isDisabled: Bool
    let dailySpending: Double
    
    
    // Display alerte for confirm daily spending
    @State private var dailyAlert = false
    let titleDailyAlert = "Retirer le coût de nourriture journalier ?"
    
    
    // Display Snackbar
    @EnvironmentObject var snackbarSettings: SnackbarSettings
    
    var body: some View {

                /// ========= DAILY SPENDING
                ///
                /// - 1. toggle alert for ask confirm,
                /// - 2. Use feedAnimals()
                /// - 3. display alertMessage to confirm
                ///
                /// > Note: if already use : edit aspect and can't use again
                
                if isDisabled == false {
                    Button {
                        dailyAlert.toggle()
                    } label: {
                        Card(image: "Harvest", text: "Gestion journalière")
                            .foregroundColor(.primary)
                    }
                    .alert(titleDailyAlert, isPresented: $dailyAlert) {
                        Button ("Confirmer") {
                            feedAnimals()
                            snackbarSettings.content = "Votre dépense à bien été prise en compte"
                            snackbarSettings.isActive = true
                        }
                        Button ("Annuler", role: .cancel) {
                            
                        }
                    } message:{
                        
                    }
                } else {
                    Card(image: "Barn", text: "Gestion journalière", active: false)
                        .opacity(0.3)
                }
            
    }
}

/// Substract dailySpending of money
/// Toggle dailySpendingAlreadyUse in true

extension DailyButtonCard {
    func feedAnimals(){
        money -= dailySpending
        isDisabled = true
    }
}

struct DailyButtonCard_Previews: PreviewProvider {
    static var previews: some View {
        DailyButtonCard(money: .constant(0.5), isDisabled: .constant(false), dailySpending: 0.5)
    }
}
