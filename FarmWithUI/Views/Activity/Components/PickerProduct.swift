//
//  PickerProduct.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 23/05/2023.
//
//  

import SwiftUI

struct PickerProduct<T:Product>: View {
    
    @State var selection : Int = 0
    @Binding var barn : Barn
    @Binding var product : T
    
    @EnvironmentObject var snackbarSettings: SnackbarSettings
    
    var onConfirmed : () -> Void

    @State private var isActive = false
    
    var body: some View {
        ZStack {
            RoundedRectangle(
                cornerSize: CGSize(width: 15, height: 15)
            )
            .frame(width: 300, height: 120)
            .shadow(radius: 1)
            .foregroundColor(.white)
            
            VStack {
                HStack {
                    Text("Quantités à ajouter :")
                        .font(.body)
                    Picker("selection", selection: $selection) {
                        ForEach(0...100, id: \.self) { select in
                            Text("\(select)")
                        }
                    }
                    .frame(width: 80.0, height: 100.0)
                    .pickerStyle(.wheel)
                }
                Button {
                    let quantity = selection
                    isActive = addQuantityOfProduct(howMany: quantity, of: product)
                    onConfirmed()
                    if isActive {
                        switch product {
                        case is Milk :
                            snackbarSettings.content = "\(quantity) litres de lait ont été ajoutés"
                        case is Wheat :
                            snackbarSettings.content = "\(quantity) tonnes de blé ont été ajoutés"
                        case is Wool :
                            snackbarSettings.content = "\(quantity) kilos de laine ont été ajoutés"
                        default:
                            snackbarSettings.content = "Une erreur s'est produite, merci de recommencer"
                        }
                    }
                    snackbarSettings.isActive = isActive
                } label: {
                    Text("Valider")
                }
                .offset(y: -20)
            }
        }
    }
}

extension PickerProduct {
    
    /// Add quantity of product
    /// - Parameters:
    ///  - howMany quantity : Double,
    ///  - of product : product.name
    /// - Returns: Bool
    func addQuantityOfProduct(howMany quantity: Int,of product: Product) -> Bool{
        switch product {
        case is Milk :
            barn.milk.quantity = barn.milk.quantity + Double(quantity)
            return true
        case is Wheat :
            barn.wheat.quantity = barn.wheat.quantity + Double(quantity)
            return true
        case is Wool :
            barn.wool.quantity = barn.wool.quantity + Double(quantity)
            return true
        default:
            return false
        }
    }
}

struct PickerProduct_Previews: PreviewProvider {
    static var previews: some View {
        PickerProduct(selection: 0, barn: .constant(Barn()), product: .constant(Barn().milk), onConfirmed: {})
    }
}
