//
//  ActivityView.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 11/05/2023.
//
//  

import SwiftUI
import AlertMessage

struct ActivityView: View {
    
    @Binding var farm : Farm
    
    // Content and text
    let image = "Barn"
    let title = "Mes activités"
    
    
    // Display form to select quantity
    @State private var showFormMilk = false
    @State private var showFormWheat = false
    @State private var showFormWool = false
    
    // Display Snackbar
    @EnvironmentObject var snackbarSettings: SnackbarSettings
    @EnvironmentObject var contentViewModel: ContentViewModel
    
    var body: some View {
        ZStack{
            Background(image: image, circle: true, textTitleContent: title, backgroundCardMode: .short,storage: true, storageMoney: farm.money, storageMilk: contentViewModel.barn.milk.quantity, storageWool: contentViewModel.barn.wool.quantity, storageWheat: contentViewModel.barn.wheat.quantity)
            VStack{
                Spacer().frame(height: 200)
            
                DailyButtonCard(money: $farm.money, isDisabled: $farm.dailySpendingAlreadyUse, dailySpending: farm.dailySpending)

                ProductButtonCard(showPicker: $showFormMilk, product: $contentViewModel.barn.milk)
                
                ProductButtonCard(showPicker: $showFormWheat, product: $contentViewModel.barn.wheat)
                
                ProductButtonCard(showPicker: $showFormWool, product: $contentViewModel.barn.wool)
            }
            .padding()
            .onChange(of: showFormMilk, perform: hideOtherPickerThanMilk)
            .onChange(of: showFormWheat, perform: hideOtherPickerThanWheat)
            .onChange(of: showFormWool, perform: hideOtherPickerThanWool)
            }
        .alertMessage(isPresented: $snackbarSettings.isActive, type: .snackbar) {
            Snackbar(content: snackbarSettings.content)
        }
        .environmentObject(snackbarSettings)
    }
    
}

extension ActivityView {
    
    func hideOtherPickerThanMilk(enable:Bool){
        if enable {
            showFormWheat = false
            showFormWool = false
        }
    }
    
    func hideOtherPickerThanWheat(enable:Bool){
        if enable {
            showFormMilk = false
            showFormWool = false
        }
    }
    
    func hideOtherPickerThanWool(enable:Bool){
        if enable {
            showFormWheat = false
            showFormMilk = false
        }
    }

}

struct ActivityView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityView(farm: .constant(Farm()))
            .environmentObject(ContentViewModel())
            .environmentObject(SnackbarSettings())
    }
}
