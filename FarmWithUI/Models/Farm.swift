//
//  Money.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 25/04/2023.
//
//  

import Foundation

struct Farm {
    var money : Double = 0
    let dailySpending : Double = 4
    var dailySpendingAlreadyUse = false
    
//    var barn: Barn = Barn()
}

struct Barn {
    var milk: Milk = Milk(price: 0.50)
    var wheat: Wheat = Wheat(price: 0.30)
    var wool: Wool = Wool(price: 1.00)
}

