//
//  Product.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 25/04/2023.
//
//  

import Foundation

protocol Product {
    var id: UUID { get set }
    var name: String { get set }
    var unit: String { get set }
    var price: Double { get set }
    var quantity: Double { get set }
    var image: String { get set }
    var action: String { get set }
}

struct Milk: Product {
    var id: UUID = UUID()
    var name: String = "Milk"
    var unit: String = "Litres"
    var price: Double
    var quantity: Double = 0
    var image: String = "Milk"
    var action: String = "Traite des vaches"
}

struct Wheat: Product{
    var id: UUID = UUID()
    var name: String = "Wheat"
    var unit: String = "Tonnes"
    var price: Double
    var quantity: Double = 0
    var image: String = "Wheat"
    var action: String = "Récolte du blé"
}

struct Wool: Product {
    var id: UUID = UUID()
    var name: String = "Wool"
    var unit: String = "Kilos"
    var price: Double
    var quantity: Double = 0
    var image: String = "Wool"
    var action: String = "Tonte des moutons"
}
