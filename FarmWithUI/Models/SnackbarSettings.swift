//
//  SnackbarSettings.swift of project named FarmWithUI
//
//  Created by Aurore D (@Tuwleep) on 26/05/2023.
//
//  

import Foundation

class SnackbarSettings: ObservableObject {
    @Published var content: String = "Une erreur s'est produite, merci de recommencer"
    @Published var isActive = false
}
